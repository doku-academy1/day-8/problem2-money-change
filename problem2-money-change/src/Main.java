import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println(moneyChange(123));
    }

    public static List<Integer> moneyChange(Integer money) {
        List<Integer> change = Arrays.asList(1, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000);
        Integer difference = money;

        List<Integer> list = new ArrayList<>();

        for (int i = change.size()-1; i>=0; i--) {
            while (difference - change.get(i) >= 0) {
                difference -= change.get(i);
                list.add(change.get(i));

            }
        }
        return list;
    }
}